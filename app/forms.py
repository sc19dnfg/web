from flask_wtf import Form
from wtforms.validators import DataRequired
from wtforms import TextField
from wtforms import IntegerField
from wtforms import BooleanField
from wtforms import FloatField
from wtforms import PasswordField


class FBook(Form):
    title = TextField('title', validators=[DataRequired()])
    description = TextField('description', validators=[DataRequired()])
    year = IntegerField('year', validators=[DataRequired()])
    author = TextField('author', validators=[DataRequired()])
    price = FloatField('price', validators=[DataRequired()])
    availability = BooleanField('availaility', validators=[DataRequired()])

class FClient(Form):
    name = TextField('title', validators=[DataRequired()])
    address = TextField('title', validators=[DataRequired()])
    email = TextField('title', validators=[DataRequired()])
    phone = TextField('title', validators=[DataRequired()])
    username = TextField('title', validators=[DataRequired()])
    password = TextField('title', validators=[DataRequired()])
    admin = BooleanField('title', validators=[DataRequired()])

class FLogin(Form):
    username = TextField('title', validators=[DataRequired()])
    password = PasswordField('title', validators=[DataRequired()])

class FRegister(Form):
    name = TextField('title', validators=[DataRequired()])
    address = TextField('title', validators=[DataRequired()])
    email = TextField('title', validators=[DataRequired()])
    phone = TextField('title', validators=[DataRequired()])
    username = TextField('title', validators=[DataRequired()])
    password = TextField('title', validators=[DataRequired()])

class FCart(Form):
    title = TextField('title', validators=[DataRequired()])
    description = TextField('description', validators=[DataRequired()])
    year = TextField('year', validators=[DataRequired()])
    author = TextField('author', validators=[DataRequired()])
    price = FloatField('price', validators=[DataRequired()])

class FPassword(Form):
    password = TextField('title', validators=[DataRequired()])
    
    